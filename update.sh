#!/bin/bash
set -euxo pipefail

TIMESTAMP=`date +"%Y-%m-%d_%H-%M-%S"`

cp ~/.ssh/authorized_keys ~/.ssh/authorized_keys.${TIMESTAMP} || true

wget -O ~/.ssh/authorized_keys https://gitlab.com/hixi/my-pub-keys/-/raw/master/authorized_keys
chmod 600 ~/.ssh/authorized_keys

if ssh-keygen -lf ~/.ssh/authorized_keys; then
    echo "authorized keys appear to be clean."
else
    echo "authorized keys appear broken, reverting. Please validate manually."
    cp ~/.ssh/authorized_keys.${TIMESTAMP} ~/.ssh/authorized_keys || true
fi
